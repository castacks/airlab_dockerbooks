#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# display usage
usage_msg="\
Usage: $(basename $0)
options:
  * optional arguments are enclosed in square brackets
  
  --config <config-file-name>
      load the project config file
  [--containers]
      Preceding option that speifies any sub-options regard to containers.
      [--stop]
        Stop all associated containers with the docker image
      [--delete]
        Delete all associated containers with the docker image
  [--images]
      Deletes all docker images in relation to the given config file.
  [--preview]
      preview the commands to runs.

examples:

    $(basename $0) --config [config-file-name] --containers --stop
    $(basename $0) --config [config-file-name] --containers --remove
    $(basename $0) --config [config-file-name] --containers --remove --image --preview

For more help, please see the README.md or wiki."

# //////////////////////////////////////////////////////////////////////////////
# load scripts

# get filename prefix (for per-project docker build)
prefix="$( cut -d '_' -f 1 <<< "$(basename $0)" )"
if [[ "${prefix}" != "$(basename $0)" ]]; then
  # prefix is set, calling from install script
  prefix="${prefix}_";
else
  # prefix is not set, calling script directly -- not from installed
  prefix=""
fi;

# load print-info
. "$(dirname $0)/${prefix}docker-utils.bash"
validate "load utils failed"

# load args
. "$(dirname $0)/${prefix}docker-args.bash"
validate "load utils failed"

# display usage message
is_display_usage "$usage_msg"

# verify env arg was set
verify_arg "$config" "Error: option '--config' must be given." "$usage_msg"

# load the env config file
script_path="$(dirname $(realpath $0))/../../dockerfiles/"
. "$script_path/$config"
validate "load config file, '$config' failed"

# validate all the config env variables have been set
validate_env_config

# set some local variables
env_path="$(dirname $script_path/$config.config)"
dockerfile_file_path="$script_path/../dockerfiles/$DOCKERFILE_PATH"
dockerfile_path="$(dirname $dockerfile_file_path)"

# prints
divider_large
title "Clean: Docker Images & Containers"

# //////////////////////////////////////////////////////////////////////////////
# docker container setup

# get the directory of this script -- "build.bash"
script_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
pushd $script_path  # go so script path

# get the image name
set_docker_image

# //////////////////////////////////////////////////////////////////////////////
# remove containers associate with image
[[ "$clean_containers" ]] && {
  # print: containers to remove
  ctns=$(docker ps -a --format '{{.Names}} {{.Image}}' | grep $image | awk '{print $1 }'; )
  text "Containers to stop and/or remove:\n";
  for name in $ctns; do display_text "\t$name"; done
  # eval: remove containers
  if [ $text == false ]; then
    # stop containers only.
    [[ "$stop_containers" ]] && {
      docker ps -a | awk '{ print $1,$2 }' | grep $image | awk '{print $1 }' | xargs -I {} docker stop {};
      validate "docker stop containers failed."; text "All containers stopped. \n"; exit_success;
    }
    docker ps -a | awk '{ print $1,$2 }' | grep $image | awk '{print $1 }' | xargs -I {} docker rm -f {};
    validate "docker clean containers failed."
    [[ "$remove_containers" ]] && {  text "All containers removed.\n"; exit_success; }
  fi
}

# //////////////////////////////////////////////////////////////////////////////
# remove docker image
[[ "$clean_images" ]] && {
  dclean="docker rmi $image"
  text "Image to remove:\n"
  display_text "\t$dclean"
  if [ $text == false ]; then eval $dclean; fi
  validate "docker remove image failed."
}

# //////////////////////////////////////////////////////////////////////////////
# cleanup & exit
popd
exit_success
