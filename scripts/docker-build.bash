#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# display usage
usage_msg="\
Usage: $(basename $0)
options:
  * optional arguments are enclosed in square brackets
  
  --config <config-file-relative path>
      relative path from [repo]/dockerfiles/ to the dockerfile configuration file
  [--force-build]
      Overwrite the existing docker image
  [--preview]
      preview docker build command; docker build is not called

  Builds a docker image from a dockerfile.

For more help, please see the README.md or wiki."

# //////////////////////////////////////////////////////////////////////////////
# load scripts

# get filename prefix (for per-project docker build)
prefix="$( cut -d '_' -f 1 <<< "$(basename $0)" )"
if [[ "${prefix}" != "$(basename $0)" ]]; then
  # prefix is set, calling from install script
  prefix="${prefix}_";
else
  # prefix is not set, calling script directly -- not from installed
  prefix=""
fi;

# load print-info
. "$(dirname $0)/${prefix}docker-utils.bash"
validate "load utils failed"

# load args
. "$(dirname $0)/${prefix}docker-args.bash"
validate "load utils failed"

# display usage message
is_display_usage "$usage_msg"

# verify env arg was set
verify_arg "$config" "Error: option '--config' must be given." "$usage_msg"

# load the env config file
script_path="$(dirname $(realpath $0))/../../dockerfiles/"
. "$script_path/$config"
validate "load config file, '$config' failed"

# validate all the config env variables have been set
validate_env_config

# set some local variables
env_path="$(dirname $script_path/$config.config)"
dockerfile_file_path="$script_path/../dockerfiles/$DOCKERFILE_PATH"
dockerfile_path="$(dirname $dockerfile_file_path)"

# prints
divider_large
title "Build: Docker Image"

# //////////////////////////////////////////////////////////////////////////////
# setup docker image & validate

# cd to the directory of this script
script_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
pushd $script_path  # cd to script path

# set the build context to the ci_tools/docker path
ctx="$script_path/../../dockerfiles/"
set_docker_image

# get the home directory
homedir=~
eval homedir=$homedir

# ssh key path
ssh_priv_key="$homedir/.ssh/$SSH_KEY"
ssh_pub_key="$homedir/.ssh/$SSH_KEY.pub"

# debug
[[ "$debug" ]] && { print_env; print_image_info; }

# check dockerfile exists
validate_file_exists "$dockerfile_file_path"

# check ssh_rsa exists
validate_file_exists "$ssh_priv_key"
validate_file_exists "$ssh_pub_key"

# //////////////////////////////////////////////////////////////////////////////
# docker build image

# expose the ssh_rsa keys in the docker build; do not change the whitespace & newline stuff.
ssh_rsa='--build-arg ssh_priv_key="$(cat '"$ssh_priv_key"')"'
ssh_rsa="$ssh_rsa "'--build-arg ssh_pub_key="$(cat '"$ssh_pub_key"')"'

# docker build command
dbuild="\
  docker build --rm --build-arg user_id=$(id -u)
    $ssh_rsa
    $BUILD_OPTIONS
    -f $dockerfile_file_path
    -t $image
    $ctx"

# check if image exists
if [[ "$(docker images -q $image 2> /dev/null)" == ""  || $force_build == true ]]; then
  # create the docker image
  warning "Docker image: '$image' does not exist, creating new image."
  display_text "$dbuild"
  # run docker build
  if [ $text == false ]; then eval $dbuild; fi;
else
  warning "\
    Docker image, '$image' already exists, not creating image.\n
     Please delete the image if you wish to use the same image name. Or use option '--force' to force overwrite."
fi
validate "docker build command failed"

# cleanup & exit
popd
exit_success
