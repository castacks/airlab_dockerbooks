#!/usr/bin/env bash

# default variables -- pre command line args
preview=false
help=false
opts=""

# arguments parser
while [[ $# -gt 0 ]]
do
  key="${1}"
  case ${key} in
  --help)
    help=true
    shift # past argument
    ;;
  help)
    help=true
    shift # past argument
    ;;
  --help)
    help=true
    shift # past argument
    ;;
  -h)
    help=true
    shift # past argument
    ;;
  --preview)
    preview=true
    shift # past argument
    ;;
  --scenario)
    scenario=${2}
    shift # past argument
    shift # past argument
    ;;
  --workspace)
    workspace=${2}
    shift # past argument
    shift # past argument
    ;;
  --env)
    env+=(${2})
    shift # past argument
    shift # past value
    ;;
  *)      # unknown option
    opts="${opts} ${1}"
    shift # past argument
    ;;
  esac
done
