#!/usr/bin/env bash

# //////////////////////////////////////////////////////////////////////////////
# print nicely

# colors
DEFCOL="\e[39m"
GREEN="\e[32m"
BLUE="\e[34m"
RED="\e[31m"
YELLOW="\e[33m"
CYAN="\e[36m"

# formatters
_(){ sed "s/^/\t/" <($*); }
line() { printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' -; }
divider() { echo -e "$GREEN" `line` "$DEFCOL\n"; }
divider_large() { printf "=======================================================================================================\n"; }
title() { echo -e "\t\t\t\t\t $GREEN == $1 == $DEFCOL \n\n"; }
text() { echo -e "$CYAN $1 $GREEN $2 $DEFCOL"; }
display_text() { echo -e "$GREEN $1 $DEFCOL\n"; }
minor_text() { echo -e "$GREEN $1 $GREEN $2 $DEFCOL"; }
warning() { echo -e "$YELLOW$1 $DEFCOL\n"; }
print_error() { echo -e "$RED$1 $DEFCOL\n"; }
exit_success() { exit 0; }
exit_failure() { exit 1; }
newline() { echo -e "\n"; }
validate() { if [ $? -ne 0 ]; then print_error "$1"; exit_failure; fi; }

# //////////////////////////////////////////////////////////////////////////////
# validate file exists or error exit
validate_file_exists() {
  if [ ! -f $1 ]; then print_error "Error: File '$1' not found!"; display_usage "$usage_msg"; exit_failure; fi; }

# silent push, pop
pushd () {
  command pushd "$@" > /dev/null
}
popd () {
  command popd "$@" > /dev/null
}
# misc.
display_usage() { echo -e "$YELLOW$1$DEFCOL\n"; }
is_display_usage() {
  # if [ $help ]; then
  if [ "$help" = true ] ; then
    display_usage "$1"
    exit_failure
  fi
}
# check if arg is set
verify_arg() { [[ -z "${1+x}" ]] && { display_usage "$usage_msg"; print_error "$2"; exit_failure; } }
validate_install() {
  [[ -z "${INFRASTRUCTURE_DOCKER_PATH+x}" ]] && {
    display_usage "$usage_msg";
    print_error "Please install infrastructure using install-docker.bash script \n ";
    exit_failure;
  }
}

# //////////////////////////////////////////////////////////////////////////////
# validate all the config file variables that must be set
validate_env_config() {
  if [ -z ${PROJECT+x} ]; then
    print_error "PROJECT config file variable is not set. Please check your config file."
    exit 1;
  fi
  if [ -z ${REPO+x} ]; then
    warning "REPO config file variable is not set. Assuming same as project dir name."
  fi
  if [ -z ${TAG+x} ]; then
    print_error "TAG config file variable is not set. Please check your config file."
    exit 1;
  fi
  if [ -z ${PROJECT+x} ]; then
    print_error "PROJECT config file variable is not set. Please check your config file."
    exit 1;
  fi
  if [ -z ${CONTAINER+x} ]; then
    print_error "CONTAINER config file variable is not set. Please check your config file."
    exit 1;
  fi
  if [ -z ${SSH_KEY+x} ]; then
    print_error "SSH_KEY config file variable is not set. Please check your config file."
    exit 1;
  fi
}
# debug prints
print_env() {
  text "== config env info ==\n"
  minor_text "PROJECT: $PROJECT"
  minor_text "REPO: $REPO"
  minor_text "TAG: $TAG"
  minor_text "CONTAINER: $CONTAINER"
  minor_text "BUILD_OPTIONS: $BUILD_OPTIONS"
  minor_text "RUN_OPTIONS: $RUN_OPTIONS"
  minor_text "JOIN_OPTIONS: $JOIN_OPTIONS"
  minor_text "SSH_KEY: $SSH_KEY"
  newline
}
print_image_info() {
  text "== image info ==\n"
  text "project:" "$proj"
  text "image:" "$image"
  text "dockerfile:" "$dockerfile.dockerfile"
  text "ctx:" "$ctx"
  text "ssh_rsa:" "$ssh_priv_key"
  newline
}

# //////////////////////////////////////////////////////////////////////////////
# misc.

# set the docker image & dockerfile name
set_docker_image() {
  # set the image name 'repo' part
  repo_prefix="$(basename $dockerfile_path)"  # prefix the image name for 'project' part

  # no repo name, change the image name
  [[ -z "$REPO" ]] && {
    # set the dockerfile name
    dockerfile="${repo_prefix}"
    # set the image name
    if [[ "$repo_prefix" != "$PROJECT" ]]; then { image="$PROJECT/$repo_prefix:$TAG"; }
    else { image="$repo_prefix:$TAG"; } fi
  }
  # have a repo name, change the image name
  [[ -z "$image" ]] && {
    # set the dockerfile name
    dockerfile="${REPO}"
    # set the image name
    if [[ "$repo_prefix" != "$REPO" && "$repo_prefix" != "$PROJECT" ]]; then { REPO="$repo_prefix-$REPO"; } fi
    image="$PROJECT/$REPO:$TAG"
  }
}
