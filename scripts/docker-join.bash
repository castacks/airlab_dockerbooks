#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# display usage
usage_msg="\
Usage: $(basename $0)
options:
  * optional arguments are enclosed in square brackets

  [ --config <config-file> ]
      load the project config file
  --name <container name>
      set the container name to join
      use '--name' or '--config', but not both.
  [--preview]
      preview docker build command; docker build is not called

  Joins the docker container. Call $(basename $0) script multiple times to join the same container.

For more help, please see the README.md or wiki."

# //////////////////////////////////////////////////////////////////////////////
# load scripts

# get filename prefix (for per-project docker build)
prefix="$( cut -d '_' -f 1 <<< "$(basename $0)" )"
if [[ "${prefix}" != "$(basename $0)" ]]; then
  # prefix is set, calling from install script
  prefix="${prefix}_";
else
  # prefix is not set, calling script directly -- not from installed
  prefix=""
fi;

# load print-info
. "$(dirname $0)/${prefix}docker-utils.bash"
validate "load utils failed"

# load args
. "$(dirname $0)/${prefix}docker-args.bash"
validate "load utils failed"

# display usage message
is_display_usage "$usage_msg"

# not given a container name, i.e. join_containers is unset, use the container name from env config
[[ -z "$join_containers" ]] && {
  # verify env arg was set
  verify_arg "$config" "Error: option '--config' must be given." "$usage_msg";

  # load the env config file
  . "$(dirname $(realpath $0))/../../dockerfiles/$config"
  validate "load config file, '$config' failed";

  # validate all the config env variables have been set
  validate_env_config
}

# prints
divider_large
title "Join: Docker Container"

# //////////////////////////////////////////////////////////////////////////////
# docker container setup

# get the directory of this script -- "build.bash"
script_path="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
pushd $script_path  # go so script path

# access control disabled, clients can connect from any host
xhost +

### docker start command
dstart="docker start $CONTAINER"

### docker exec command
dexec="\
  docker exec 
    --privileged
    -e DISPLAY=${DISPLAY}
    -e LINES=`tput lines`
    ${JOIN_OPTIONS}
    -it ${CONTAINER} /bin/bash"

# //////////////////////////////////////////////////////////////////////////////
# docker start & join

# container dne -- fail exit
if [[ "$(docker ps -a | grep $CONTAINER)" == "" ]]; then
  print_error "Docker container, '$CONTAINER' does not exist. Please check the $config.config file."
  xhost -
  exit_failure;
fi

# start the container
if [[ "$(docker inspect -f {{.State.Running}}  $CONTAINER)" == false ]]; then
  warning "Docker container, '$CONTAINER' is not started, starting the container."
  text "docker start: " "$dstart"
  if [ $text == false ]; then eval $dstart; fi
  validate "docker start command failed"
fi

# join the container
warning "Docker container, '$CONTAINER' exists, joining the container."
display_text "$dexec"
if [ $text == false ]; then eval $dexec; fi
validate "docker join command failed"

# cleanup & exit
# access control enabled, only authorized clients can connect
newline
xhost -
popd
exit_success
