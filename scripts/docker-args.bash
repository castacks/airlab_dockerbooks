#!/usr/bin/env bash

# default variables -- pre command line args
text=false
force_build=false
# non_nvidia=false
opts=""
# arguments
while [[ $# -gt 0 ]]
do
  key="${1}"
  case ${key} in
  --config)
    config="${2}"
    shift # past argument
    shift # past value
    ;;
  --preview)
    text=true
    shift # past argument
    ;;
  --force)
    force_build=true
    shift # past argument
    ;;
  help)
    help=true
    shift # past argument
    ;;
  -h)
    help=true
    shift # past argument
    ;;
  --help)
    help=true
    shift # past argument
    ;;
  --debug)
    debug=true
    shift # past argument
    ;;
  --no-nvidia)
    no_nvidia=true
    shift # past argument
    ;;
  --images)
    clean_images=true
    shift # past argument
    ;;
  --containers)
    clean_containers=true
    shift # past argument
    ;;
  --stop)
    stop_containers=true
    shift # past argument
    ;;
  --remove)
    remove_containers=true
    shift # past argument
    ;;
  --name)
    join_containers=true
    CONTAINER="${2}"
    shift # past argument
    shift # past value
    ;;
  *)      # unknown option
    shift # past argument
    ;;
  esac
done
