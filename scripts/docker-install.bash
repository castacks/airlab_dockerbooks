#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# display usage

# named variables: script filepath, package name, install script path
file_path=$BASH_SOURCE
usage_msg=$1
PKG=$2
SCRIPT_PATH="$(dirname $file_path)"
INSTALL_SCRIPTS_DIR="$SCRIPT_PATH/../install-scripts/"

# exit
exit_failure() { echo "Running script disabled."; exit 1; }
[[ $0 != $BASH_SOURCE ]] || exit_failure

# //////////////////////////////////////////////////////////////////////////////

# load print-info
. "$SCRIPT_PATH/docker-utils.bash"
validate "load utils failed"

# create links to scripts for enable run-anywhere mode
create_script_links() {
  # remove previous links
  rm -rf $INSTALL_SCRIPTS_DIR
  mkdir $INSTALL_SCRIPTS_DIR
  # add links to docker un-prefixed docker scripts
  for filename in $SCRIPT_PATH/*; do
    ln -s $(realpath $filename) $INSTALL_SCRIPTS_DIR/${PKG}_$(basename $filename)
  done
}

# remove links to scripts for enable run-anywhere mode
remove_script_links() {
  # remove previous links
  rm -rf $INSTALL_SCRIPTS_DIR
  mkdir $INSTALL_SCRIPTS_DIR
}

# add script path from zsh/bash config
add_to_config() {
  # add to config
  echo "export ${PKG}_DOCKER_PATH=$(realpath $INSTALL_SCRIPTS_DIR)" >> /home/$USER/.$1
  echo "export PATH=\$PATH:\$${PKG}_DOCKER_PATH" >> /home/$USER/.$1
}

# remove script path from zsh/bash config
remove_from_config() {
  # remove from config
  sed -i "/${PKG}_DOCKER_PATH/d" /home/$USER/.$1
  sed -i "/\$${PKG}_DOCKER_PATH/d" /home/$USER/.$1
}

# install 
install() {
  # update the submodules
  git submodule init
  # git submodule update --init --recursive

  # remove any previous alias
  remove_script_links
  remove_from_config "zshrc"
  remove_from_config "bashrc"
  
  # script add to zsh, bash configs
  create_script_links
  add_to_config "zshrc"
  add_to_config "bashrc"
}

# uninstall
uninstall() {
  # remove any previous alias
  remove_script_links
  remove_from_config "zshrc"
  remove_from_config "bashrc"
}


