#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# docker entrypoint script
# //////////////////////////////////////////////////////////////////////////////
set -e

# log message
echo " == Deployer Commander == "

# setup roscore IP/hostnames and source any workspaces
_SET_ROSCORE=true
_SET_WS=true
_ROS_WS="\$ws_devel_source"
source /docker-entrypoint/roscore-env-setup.bash

# source the bashrc to set the added ros variables
source ~/.bashrc

# build the repos
echo " == Deployer Commander Run =="
cd ~/deploy_ws/src/

# install the deploy repo configs
./install-deployer.bash --install

# start launch script
start() {
  # run the build command
  ./deployer -r ${deploybook_robot}.${deploybook_run}.start
}

# stop launch script
stop() {
  echo "Container stopped, performing cleanup...";
  ./deployer -r ${deploybook_robot}.${deploybook_run}.stop
}

#Trap SIGTERM
trap 'stop' SIGTERM

# start the launch
start

# Entrypoint ended work.
echo "Deployer commander completed all run start commands.";

# leave launch open to allow tmux access
# /bin/bash "$@"

# wait forever, catch exit
while true
do
  tail -f /dev/null & wait ${!}
done

# Entrypoint ended work.
echo "Entrypoint ended";

