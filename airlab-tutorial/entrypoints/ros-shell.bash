#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# docker entrypoint script
# //////////////////////////////////////////////////////////////////////////////
set -e

# log message
echo " == ROS Shell == "

# setup roscore IP/hostnames and source any workspaces
_SET_ROSCORE=true
_SET_WS=true
_ROS_WS="/opt/ros/melodic/setup.bash"
source /docker-entrypoint/roscore-env-setup.bash

# source the bashrc to set the added ros variables
source ~/.bashrc

# Disallow docker exit -- keep container running
echo "Entrypoint ended";
/bin/bash "$@"
