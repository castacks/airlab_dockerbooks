#!/usr/bin/env bash
# //////////////////////////////////////////////////////////////////////////////
# docker entrypoint script
# //////////////////////////////////////////////////////////////////////////////
set -e

# log message
echo " == Deployer Commander == "

# setup roscore IP/hostnames and source any workspaces
_SET_ROSCORE=true
_SET_WS=true
_ROS_WS="\$ws_devel_source"
source /docker-entrypoint/roscore-env-setup.bash

# source the bashrc to set the added ros variables
source ~/.bashrc

# build the repos
echo " == Deployer Commander Run =="
cd ~/deploy_ws/src/

# install the deploy repo configs
./install-deployer.bash --install

# run the build command
./deployer -r ${deploybook_robot}.${deploybook_run}

# TODO: if error, catch & stay open.

# Entrypoint ended work.
echo "Entrypoint ended";
